﻿using FanalcaBusiness.BusinessLogic;
using FanalcaEF;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FanalcaApi.Controllers
{
    [Route("api/controller")]
    [ApiController]
    public class EmpleadoController : ControllerBase
    {
        EmpleadoBusiness empleado = new EmpleadoBusiness();
       

        [Route("obtenerEmpleado/{nombre}")]
        [HttpGet]
        public ActionResult obtenerEmpleadoPorNombre(string nombre)
        {
            List<FANALCA_EMPLEADO> resultado = empleado.obtenerEmpleadoPorNombre(nombre);
            return Ok(resultado);
        }

        [Route("registrarEmpleado")]
        [HttpPost]
        public ActionResult registrarEmpleado([FromBody] FANALCA_EMPLEADO data)
        {
             empleado.registrarEmpleado(data);
            return Ok("ok");
        }

        [Route("ActualizarEmpleado")]
        [HttpPost]
        public ActionResult ActualizarEmpleado([FromBody] FANALCA_EMPLEADO data)
        {
            empleado.actualizarEmpleado(data);
            return Ok("ok");
        }

        [Route("EliminarEmpleado")]
        [HttpPut]
        public ActionResult EliminarEmpleado(decimal consEmpleado)
        {
            empleado.EliminarEmpleado(consEmpleado);
            return Ok("ok");
        }

        [Route("obtenerEmpleadoPorId/{id}")]
        [HttpGet]
        public ActionResult obtenerEmpleadoPorId(decimal  id)
        {
           obtenerEmpleadoPorDocumento_Result resultado = empleado.obtenerEmpleadoPorDocumento(id);
            return Ok(resultado);
        }
    }
}
