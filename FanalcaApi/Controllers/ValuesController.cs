﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FanalcaBusiness.BusinessLogic;
using FanalcaEF;
using Microsoft.AspNetCore.Mvc;

namespace FanalcaApi.Controllers
{
    [Route("api/controller")]
    [ApiController]
 
    public class ValuesController : ControllerBase
    {
        AreaBusiness area = new AreaBusiness();

       
        [Route("obtenerTodosArea")]
        [HttpGet]
        [Authorize]
        public ActionResult obtenerTodosArea()
        {
            List<FANALCA_AREA> resultado = area.obtenerTodosArea();
            return Ok(resultado);
        }

        [Route("obtenerSubAreasPorArea/{consArea}")]
        [HttpGet]
        [Authorize]
        public ActionResult obtenerSubAreasPorArea(decimal consArea)
        {
            List<FANALCA_SUB_AREA> resultado = area.obtenerSubAreasPorArea(consArea);
            return Ok(resultado);
        }




    }
}
