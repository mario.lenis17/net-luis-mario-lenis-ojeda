﻿using FanalcaData.FanalcaData.BusinessLogic;
using FanalcaEF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FanalcaBusiness.BusinessLogic
{
    public class AreaBusiness
    {
        public AreaData datos = new AreaData();
        public List<FANALCA_AREA> obtenerTodosArea()
        {
           return datos.obtenerTodosArea();
        }
        public List<FANALCA_SUB_AREA> obtenerSubAreasPorArea(decimal consArea)
        {
            return datos.obtenerSubAreasPorArea(consArea);
        }
    }
}
