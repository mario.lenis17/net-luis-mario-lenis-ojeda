﻿using FanalcaData.FanalcaData.BusinessLogic;
using FanalcaEF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FanalcaBusiness.BusinessLogic
{
    public class EmpleadoBusiness
    {
        EmpleadoData empleado = new EmpleadoData();

        public List<FANALCA_EMPLEADO> obtenerEmpleadoPorNombre(string nombre)
        {
            return empleado.obtenerEmpleadoPorNombre(nombre);
        }
        public void registrarEmpleado(FANALCA_EMPLEADO registro)
        {
            empleado.registrarEmpleado(registro);
        }
        public void actualizarEmpleado(FANALCA_EMPLEADO registro)
        {
            empleado.actualizarEmpleado(registro);
        }
        public void EliminarEmpleado(decimal consEmpleado)
        {
            empleado.EliminarEmpleado(consEmpleado);
        }
        public obtenerEmpleadoPorDocumento_Result obtenerEmpleadoPorDocumento(decimal documento)
        {
            return empleado.obtenerEmpleadoPorDocumento(documento);
        }

    }
}
