﻿using FanalcaEF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FanalcaData.FanalcaData.BusinessLogic
{
   public class AreaData
    {
        public List<FANALCA_AREA> obtenerTodosArea()
        {
            using (FanalcaEntities context = new FanalcaEntities())
            {
                return context.FANALCA_AREA
                .OrderBy(t => t.Area)
                .ToList();
            }
        }

        public List<FANALCA_SUB_AREA> obtenerSubAreasPorArea(decimal consArea)
        {
            using (FanalcaEntities context = new FanalcaEntities())
            {
                return context.FANALCA_SUB_AREA
                    .Where(x=>x.Cons_Area== consArea)
                .OrderBy(t => t.Descripcion)
                .ToList();
            }
        }
    }
}
