﻿using FanalcaEF;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace FanalcaData.FanalcaData.BusinessLogic
{
   public class EmpleadoData
    {
        public List<FANALCA_EMPLEADO> obtenerEmpleadoPorNombre(string nombre)
        {
            using (FanalcaEntities context = new FanalcaEntities())
            {
                return context.FANALCA_EMPLEADO
                .Where(x=>x.Nombres== nombre)
                .ToList();
            }

        }
        public void registrarEmpleado(FANALCA_EMPLEADO empleado)
        {
            using(FanalcaEntities context= new FanalcaEntities())
            {
             
                        context.FANALCA_EMPLEADO.Add(empleado);
                        context.SaveChanges();

            }
        }
        public void actualizarEmpleado(FANALCA_EMPLEADO empleado)
        {
            using (FanalcaEntities context = new FanalcaEntities())
            {

                context.FANALCA_EMPLEADO.Add(empleado);
                context.SaveChanges();

            }
        }
        public void EliminarEmpleado(decimal empleado)
        {
            using (FanalcaEntities context = new FanalcaEntities())
            {

                var employer = new FANALCA_EMPLEADO { Cons_Empleado = empleado };
                context.Entry(employer).State = EntityState.Deleted;
                context.SaveChanges();

            }
        }
        public obtenerEmpleadoPorDocumento_Result obtenerEmpleadoPorDocumento(decimal documento)
        {
            
            using (FanalcaEntities context = new FanalcaEntities())
            {
                return context.obtenerEmpleadoPorDocumento(documento).FirstOrDefault();

            }
            
        }
    }
}
