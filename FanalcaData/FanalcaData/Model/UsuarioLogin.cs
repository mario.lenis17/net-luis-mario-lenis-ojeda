﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FanalcaData.FanalcaData.Model
{
    public class UsuarioLogin
    {
        public string Usuario { get; set; }
        public string Password { get; set; }
    }
}
